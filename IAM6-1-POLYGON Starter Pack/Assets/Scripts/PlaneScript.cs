﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
    public GameObject[] waypoints;
    public int num = 0;

    public float minDist;
    public float speed;

    public bool go = true;

    void Start()
    {

    }


    void Update()
    {
        float dist = Vector3.Distance(gameObject.transform.position, waypoints[num].transform.position);

        if (go)
        {
            if (dist > minDist)
            {
                Move();
            }
            else
            {
                if (num + 1 == waypoints.Length)
                {
                    num = 0;
                }
                else
                {
                    num++;
                }
            }
        }
    }

    public void Move()
    {
        gameObject.transform.LookAt(waypoints[num].transform.position);
        gameObject.transform.position += gameObject.transform.forward * speed * Time.deltaTime;
    }
}