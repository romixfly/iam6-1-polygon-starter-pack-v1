﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coin_sound : MonoBehaviour
{
    private AudioSource source;
    public GameObject coin;
    public bool collided = false;
    public GameObject particles;

    void Start()
    {
        source = gameObject.GetComponent<AudioSource>();
    }
    private void OnTriggerEnter(Collider other)
    {
       if (!collided)
       {
           if (other.tag == "Player")
           {
               source.Play();
               particles.GetComponent<ParticleSystem>().Play();
               Destroy(coin);
               collided = true;
           }
       }
       
       
    }
}
