﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class treerandom : MonoBehaviour
{
    public AudioSource treeAudioSource;
    public AudioClip treeAudioclip;

    // Start is called before the first frame update
    void Start()
    {
        Randomization();
        treeAudioSource = gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Randomization()

    {
        treeAudioSource.volume = Random.Range(0.8f, 1f);
        treeAudioSource.pitch = Random.Range(0.8f, 1.2f);
    }
}
