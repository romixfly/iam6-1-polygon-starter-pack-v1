﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class footsteps : MonoBehaviour
{
    AudioSource audiosource;
    public AudioClip[] runnigclips; 
    public AudioClip[] walkingclips; 
    vThirdPersonInput tpInput; 
    vThirdPersonController tpController;
    public float VolumeMin = 1f;
    public float pitchMin = 1f;
    public float pitchMax = 1f;
    int lastIndex; 
    int newIndex;

    void Start()
    {
        audiosource = GetComponent<AudioSource>();
        tpInput = GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();
    }
    void footstep()
    {
        if (tpInput.cc.inputMagnitude > 0.1 )
        {
           audiosource.volume = Random.Range(VolumeMin, 1f);
           audiosource.pitch = Random.Range(pitchMin, pitchMax);
            if (tpController.isSprinting)
            {
                Randomization(runnigclips.Length);
                audiosource.PlayOneShot(runnigclips[newIndex]);
                lastIndex = newIndex;
            }
           else
            {
                Randomization(walkingclips.Length);
                audiosource.PlayOneShot(walkingclips[newIndex]);
                lastIndex = newIndex;
            }                       
        }

    }
    void Randomization(int cliplenght)
    {
       newIndex = Random.Range(0, cliplenght);
       while (newIndex == lastIndex)
       newIndex = Random.Range(0, cliplenght);
    }
   
}